# Flask Tutorial

Run the flask server:
```
env FLASK_APP=flaskblog.py flask run
```

Run the flask server in debug mode:
```
env FLASK_APP=flaskblog.py env FLASK_DEBUG=1 flask run
```

Just simple run:
```
python flaskblog.py
```

## SQLAlchemy
Install:
```
pip install sqlalchemy
pip install flask-sqlalchemy
```

Write into database:
```
from flaskblog import db
db.create_all()
from flaskblog import User, Post
user1 = User(username="Viktor", email="viktor@gmail.com", password="password")
db.session.add(user1)
user2 = User(username="John Doe", email="jd@gmail.com", password="password")
db.session.add(user2)
db.session.commit()

post1 = Post(title="Blog1", content="First post Content!", user_id=user.id)
db.session.add(post1)
db.session.commit()
```

Query database:
```
User.query.all()
User.query.first() == User.query.all()[0]
User.query.filter_by(username="Viktor").all()
user = User.query.filter_by(username="Viktor").first()
user.id
user = User.query.get(1)
user.posts

post = Post.query.first()
post.user_id
```

Delete all data in the database:
```
db.drop_all()
```